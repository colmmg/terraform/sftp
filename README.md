# sftp
Deploy a serverless SFTP server with AWS.

# Terraform Deploy Instructions
To deploy run:
```
terraform init
terraform apply
```

# Post Deploy Update SSH Host Key
After the stack deploys you can update the SSH host key with the following:
```
aws transfer update-server --server-id `terraform state show aws_transfer_server.transfer |grep "= \"s-" |grep -v "amazonaws.com" |cut -d '=' -f2 |xargs` --host-key file://~/rsa-host-key
```

# References
The following articles were referenced in the creation of this code:
* [Enable password authentication for AWS Transfer for SFTP using AWS Secrets Manager - by Warren Paull](https://aws.amazon.com/blogs/storage/enable-password-authentication-for-aws-transfer-for-sftp-using-aws-secrets-manager/)
* [Lift and shift migration of SFTP servers to AWS - by Wayne Mesard](https://aws.amazon.com/blogs/storage/lift-and-shift-migration-of-sftp-servers-to-aws/)
* [Using AWS SFTP Logical Directories to Build a Simple Data Distribution Service - by Jason Barto and Jeff Bartley](https://aws.amazon.com/blogs/storage/using-aws-sftp-logical-directories-to-build-a-simple-data-distribution-service/)
